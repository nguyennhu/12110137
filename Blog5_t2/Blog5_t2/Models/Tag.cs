﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog5_t2.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "10-100 ky tu", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}