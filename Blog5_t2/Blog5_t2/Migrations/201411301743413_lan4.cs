namespace Blog5_t2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Titl", c => c.String(nullable: false, maxLength: 500));
            DropColumn("dbo.Posts", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            DropColumn("dbo.Posts", "Titl");
        }
    }
}
