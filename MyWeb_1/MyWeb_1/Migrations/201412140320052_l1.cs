namespace MyWeb_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.Posts", new[] { "UserProfile_UserId" });
            RenameColumn(table: "dbo.Posts", name: "UserProfile_UserId", newName: "UserProfileUserId");
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AddForeignKey("dbo.Posts", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.Posts", "UserProfileUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Posts", "UserProfileUserId", "dbo.UserProfile");
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            RenameColumn(table: "dbo.Posts", name: "UserProfileUserId", newName: "UserProfile_UserId");
            CreateIndex("dbo.Posts", "UserProfile_UserId");
            AddForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}
