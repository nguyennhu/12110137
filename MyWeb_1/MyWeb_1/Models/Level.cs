﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb_1.Models
{
    public class Level
    {
        public int ID { set; get; }
        public String Le_Name { set; get; }
        public String Discription { set; get; }
        public bool IsActive { set; get; }
        public virtual ICollection<Lesson> Lessons { set; get; }

    }
}