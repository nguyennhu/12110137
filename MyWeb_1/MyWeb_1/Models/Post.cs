﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MyWeb_1.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "20-500 ky tu", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Toi thieu 50 ky tu!!!", MinimumLength = 50)]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}