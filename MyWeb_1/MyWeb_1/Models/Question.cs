﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb_1.Models
{
    public class Question
    {
        public int ID { set; get;}
        public int AccountID { set; get; }

        public int LevelID { set; get; }
        public int LessonID { set;get;}
        public virtual Lesson Lesson { set; get; }
        public String Ques_Title { set; get; }
        public String Ques_Body { set; get; }
        public DateTime DateCreated { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        
        
    }
}