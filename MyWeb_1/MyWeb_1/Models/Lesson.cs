﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWeb_1.Models
{
    public class Lesson
    {
        public int ID { set; get; }
        public String Les_Name { set; get; }
        public String Les_Discription { set; get; }
        public bool IsActive { set; get; }
        
        public int LevelID { set; get; }
        public virtual Level Level { set; get; }
        public virtual ICollection<Question> Questions { set; get; }
        
       
    }
}