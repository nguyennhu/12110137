namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan41 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String());
        }
    }
}
