﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required] // phai nhap du lieu cho nhung phan o phia duoi
        [StringLength(100, ErrorMessage = "So ky tu phai tu 10-100", MinimumLength = 10)]
        public string Content { set; get; }
        
        // quan he: 1 tag co the co nhieu post
        public virtual ICollection<Post> Posts { set; get; }
    }
}