﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }
        public string Author { set; get; }
        // khoi tao lien ket giua ppostID va comment
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}