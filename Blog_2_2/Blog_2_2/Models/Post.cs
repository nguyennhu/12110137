﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("BaiViet")]
    public class Post
    {
        [Range(2,100)]  //gioi han gia tri nhap vao
        // nhap ID vao
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]  // danh dau nhung truong bat buoc phai nhap du lieu
        public string Title { set; get; }
        // gioi han so ky tu nhap vao
        [StringLength(250,ErrorMessage="So luong ky trong khoang 10-250",MinimumLength=10)]
        public string Body { set; get; }
       
        // dinh dang kieu du lieu nhap vao
        // dinh nghia kieu du lieu cua no la ngay
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateUpdate { set; get; }

        // tao moi quan he giua 1 post co the co nhieu comment va nhieu tag
        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

       
    }
}