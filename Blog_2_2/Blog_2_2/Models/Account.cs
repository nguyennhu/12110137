﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Account
    {
        public int ID { set; get; }
        [Required]  // danh dau nhung truong phai nhap du lieu

        [DataType(DataType.Password)]
        public string Password { set; get; }
        [StringLength(60,ErrorMessage="Password tu 8-60 ky tu!!!",MinimumLength=8)]

        [DataType(DataType.EmailAddress)]
        public string Email { set; get; }
        public string Firstname { set; get; }
        public string Lastname { set; get; }


        public virtual ICollection<Post> Posts { set; get; }
    }
}