﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_5_Account.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "10 - 100 ki tu", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }

       
    }
}