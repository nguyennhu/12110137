﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_5_Account.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "20 - 500 ki tu", MinimumLength = 20)]
        public String Title { set; get; }
        
        [Required]
        [MinLength(50, ErrorMessage = "toi thieu 50 ki tu")]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}