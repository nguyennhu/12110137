﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_5_Account.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]

        public DateTime DataCreated { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DataCreated).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}