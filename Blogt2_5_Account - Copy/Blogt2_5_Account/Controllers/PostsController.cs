﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blogt2_5_Account.Models;

namespace Blogt2_5_Account.Controllers
{
    [Authorize] //cho biet dang nhap vao, phai duoc dang nhap vao
    public class PostsController : Controller
    {
        private BlogDbContext db = new BlogDbContext();
       
        // GET: /Posts/
        [AllowAnonymous]// khong dang nhap van xem duoc nhung phuong thuc nay
        //
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile);
            return View(posts.ToList());
        }

        //
        // GET: /Posts/Details/5
        [AllowAnonymous]// khong dang nhap van xem duoc nhung phuong thuc nay
        //
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Posts/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Posts/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                // tao cac list Tag
                List<Tag> Tags = new List<Tag>();
                // tach cac tag theo dau ','
                string[] TagContent = Content.Split(',');
                // lap cac Tag vua tach
                foreach(string item in TagContent)
                {
                    //tim xem Taag content da co hay chua
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if(ListTag.Count()>0)
                    {
                        //neu co tag roi thi add them vao post
                        tagExits=ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        //neu chua co thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vao list cac Tag
                    Tags.Add(tagExits);
                }
                //gan list Tag cho post
                post.Tags = Tags;
                int userid=db.UserProfiles.Select(x=>new{x.UserId, x.UserName})
                    .Where(y=>y.UserName==User.Identity.Name).Single().UserId;
                post.UserProfileUserId=userid;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Posts/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // POST: /Posts/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Posts/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Posts/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}