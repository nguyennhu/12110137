﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blogt2_5_Account.Models;

namespace Blogt2_5_Account.Controllers
{
    public class TagsController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Tags/

        public ActionResult Index()
        {
            return View(db.Tags.ToList());
        }

        //
        // GET: /Tags/Details/5

        public ActionResult Details(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // GET: /Tags/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tags/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tag tag)
        {
            if (ModelState.IsValid)
            {
                db.Tags.Add(tag);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tag);
        }

        //
        // GET: /Tags/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // POST: /Tags/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tag tag)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tag).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tag);
        }

        //
        // GET: /Tags/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tag tag = db.Tags.Find(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        //
        // POST: /Tags/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tag tag = db.Tags.Find(id);
            db.Tags.Remove(tag);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        //
        //GET: Tag/Search
        public ActionResult Search(int id=0)
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(Tag tag, string content)
        {
            List<Post> posts = new List<Post>();
            string TagContent = content;
            Tag tagExits = null;
            foreach(var tags in db.Tags)
            {
                if(tags.Content==content)
                {
                    tagExits = tags;
                    break;
                }
            }
            return RedirectToAction("Searched/" + tagExits.ID);

        }
        //
        //GET:/Tag/Seached
        public ActionResult Searched(int id=0)
        {
            Tag tag = db.Tags.Find(id);
            if(tag==null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }
    }
}