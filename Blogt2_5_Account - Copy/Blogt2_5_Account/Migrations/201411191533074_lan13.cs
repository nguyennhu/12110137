namespace Blogt2_5_Account.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "TagID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tags", "TagID");
        }
    }
}
