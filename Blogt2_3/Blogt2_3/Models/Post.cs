﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_3.Models
{
    public class Post
    {
        
        public int ID { set; get; }
        [StringLength(100, ErrorMessage = "So ky tu tu 10-100", MinimumLength = 10)]
        public String Title { set; get; }
        [StringLength(100,ErrorMessage="So ky tu tu 10-100",MinimumLength=10)]
        public String Body { set; get; }
        
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}