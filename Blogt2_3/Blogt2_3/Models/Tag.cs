﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_3.Models
{
    public class Tag
    {
        
        public int ID { set; get; }
        
        [StringLength(100, ErrorMessage = "So ky tu 10-100", MinimumLength = 10)]
        public String Author { set; get; }
        
        public virtual ICollection<Post> Posts { set; get; }

    }
}