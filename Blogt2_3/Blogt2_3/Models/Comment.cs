﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogt2_3.Models
{
    public class Comment
    {

        public int ID { set; get; }
    //    [StringLength(100, ErrorMessage = "So ky tu 10-100", MinimumLength = 10)]
        public String Body { set; get; }
        
        
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }
        //tinh thoi gian tao
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DataCreated).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}