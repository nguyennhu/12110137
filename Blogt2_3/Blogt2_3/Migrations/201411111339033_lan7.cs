namespace Blogt2_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(maxLength: 100));
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 100));
            AlterColumn("dbo.Tags", "Author", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
        }
    }
}
