namespace Blogt2_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan8 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tags", "Author", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Author", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
