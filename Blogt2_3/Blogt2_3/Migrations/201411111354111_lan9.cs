namespace Blogt2_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 100));
        }
    }
}
