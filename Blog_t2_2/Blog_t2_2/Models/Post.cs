﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_t2_2.Models
{
    public class Post
    {
        [Key]
        public int Id { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }

    }
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }

}