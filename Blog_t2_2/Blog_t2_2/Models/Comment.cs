﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t2_2.Models
{
    public class Comment
    {
        [Key]
        public int Id { set; get; }
        public int Post_Id { set; get; }
        public string Body { set; get; }
    }
}